
%debug
%verbose	/*generate file: tiger.output to check grammar*/
%{
/*

PROGRAMMER: L. K. Silkeutsabay
PROGRAM #: 3
DUE DATE: Wednesday, 9302020
INSTRUCTOR: Dr. Zhijiang Dong

*/
#include <iostream>
#include <string>
#include "ErrorMsg.h"
#include <FlexLexer.h>

using std::string;
using std::stringstream;



int yylex(void);		/* function prototype */
void yyerror(char *s);	//called by the parser whenever an eror occurs

%}

%union {
	int		ival;	//integer value of INT token
	std::string* sval;	//pointer to name of IDENTIFIER or value of STRING
					//I have to use pointers since C++ does not support
					//string object as the union member
}

/* TOKENs and their associated data type */
%token <sval> ID STRING
%token <ival> INT

%token
  COMMA COLON SEMICOLON LPAREN RPAREN LBRACK RBRACK
  LBRACE RBRACE DOT
  ARRAY IF THEN ELSE WHILE FOR TO DO LET IN END OF
  BREAK NIL
  FUNCTION VAR TYPE


/* add your own predence level of operators here */
%nonassoc ASSIGN
%left OR
%left AND
%nonassoc EQ NEQ LE GE LT GT
%left MINUS PLUS
%left TIMES DIVIDE
%nonassoc UMINUS
%left DOT


%start program

%%

/* This is a skeleton grammar file, meant to illustrate what kind of
 * declarations are necessary above the %% mark.  Students are expected
 *  to replace the two dummy productions below with an actual grammar.
 */

program	:	exp

exp:  STRING
    | INT
    | NIL
    | lvalue
    | UMINUS exp
    | exp PLUS exp
    | exp MINUS exp
    | exp TIMES exp
    | exp DIVIDE exp
    | exp EQ exp
    | exp NEQ exp
    | exp LT exp
    | exp GT exp
    | exp LE exp
    | exp GE exp
    | exp AND exp
    | exp OR exp
    | lvalue ASSIGN exp
    | ID LPAREN explist RPAREN
    | LPAREN explist RPAREN
    | ID LBRACE fieldlist RBRACE
    | ID LBRACK exp RBRACK OF exp
    | IF exp THEN exp
    | IF exp THEN exp ELSE exp
    | WHILE exp DO exp
    | FOR ID ASSIGN exp TO exp DO exp
    | BREAK
    | LET declist IN expseq END
    | LET declist IN END
    ;

binop:
      PLUS
    | MINUS
    | TIMES
    | DIVIDE
    | EQ
    | NEQ
    | LT
    | GT
    | LE
    | GE
    | AND
    | OR
    ;

explist:
      exp
    | explist COMMA exp
    ;

expseq: /* empty */
    |  exp
    | expseq SEMICOLON exp
    ;

fieldlist:
      ID EQ exp
    | fieldlist COMMA ID EQ exp
    ;

lvalue:
      ID
    | lvalue DOT ID
    | lvalue LBRACK exp RBRACK
    ;

declist:
      dec
    | declist dec
    ;

dec:
      typedec
    | variabledec
    ;

typedec:
      TYPE ID EQ typenamedef
    ;

typenamedef:
      ID
    | LBRACE typefields RBRACE
    | ARRAY OF ID
    ;

typefields:
      typefield
    |  typefields COMMA typefield
    ;

typefield:
      ID COLON ID
    ;

variabledec:
       VAR ID ASSIGN exp
    |  VAR ID COLON ID ASSIGN exp
    ;


%%
extern yyFlexLexer	lexer;
int yylex(void)
{
	return lexer.yylex();
}

void yyerror(char *s)
{
	extern int	linenum;			//line no of current matched token
	extern int	colnum;
	extern void error(int, int, std::string);

	error(linenum, colnum, s);
}

