%option noyywrap
%option c++

%{
/*

PROGRAMMER: L. K. Silkeutsabay
PROGRAM #: 3
DUE DATE: Wednesday, 9302020
INSTRUCTOR: Dr. Zhijiang Dong

*/
#include <iostream>
#include <string>
#include <sstream>
#include "tiger.tab.hh"
#include "ErrorMsg.h"

using std::string;
using std::stringstream;

ErrorMsg			errormsg;	//error handler

int		comment_depth = -1;	// depth of the nested comment
string	value = "";			// the value of current string

int			beginLine=-1;	//beginning line no of a string or comment
int			beginCol=-1;	//beginning column no of a string or comment

int		linenum = 1;		//beginning line no of the current matched token
int		colnum = 1;			//beginning column no of the current matched token
int		tokenCol = 1;		//column no after the current matched token

//the following defines actions that will be taken automatically after
//each token match. It is used to update colnum and tokenCol automatically.
#define YY_USER_ACTION {colnum = tokenCol; tokenCol=colnum+yyleng;}

int string2int(string);			//convert a string to integer value
void newline(void);				//trace the line #
void error(int, int, string);	//output the error message referring to the current token
%}

/* regexes definition */
ALPHA		[A-Za-z]
DIGIT		[0-9]
INT			[0-9]+
IDENTIFIER	{ALPHA}(({ALPHA}|{DIGIT}|"_")*)


/* exclusive start conditions for STRING and COMMENT cases */
%x IN_STRING
%x IN_COMMENT


%% // handle basic tokens
" "				{}
\t				{}
\b				{}
\n				{newline(); }
"while"                 { return WHILE; }
"for"                   { return FOR; }
"to"                   { return TO; }
"break"                   { return BREAK; }
"let"                   { return LET; }
"in"                   { return IN; }
"end"                   { return END; }
"function"                   { return FUNCTION; }
"var"                   { return VAR; }
"type"                   { return TYPE; }
"array"                   { return ARRAY; }
"if"                   { return IF; }
"then"                   { return THEN; }
"else"                   { return ELSE; }
"do"                   { return DO; }
"of"                   { return OF; }
"nil"                   { return NIL; }

","                     { return COMMA; } // handle punctuation symbols
":"                     { return COLON; }
";"                     { return SEMICOLON; }
"("                     { return LPAREN; }
")"                     { return RPAREN; }
"["                     { return LBRACK; }
"]"                     { return RBRACK; }
"{"                     { return LBRACE; }
"}"                     { return RBRACE; }
"."                     { return DOT; }
"+"                     { return PLUS; }
"-"                     { return MINUS; }
"*"                     { return TIMES; }
"/"                     { return DIVIDE; }
"="                     { return EQ; }
"<>"                     { return NEQ; }
"<" 			{ return LT; }
"<=" 			{ return LE; }
">" 			{ return GT; }
">=" 			{ return GE; }
"&" 			{ return AND; }
"|" 			{ return OR; }
":="                    { return ASSIGN; }

{IDENTIFIER} 	{ value = YYText(); yylval.sval = new string(value); return ID; }
{INT}		 	{ yylval.ival = string2int(YYText()); return INT; }


\" { BEGIN(IN_STRING); value = ""; beginLine = linenum; beginCol = colnum; } // handle strings, clear value variable
<IN_STRING>\"              { BEGIN (0); yylval.sval = new string(value); return STRING; } // string ending - copy gathered value into sval. return STRING token
<IN_STRING>\\n             { value += "\n"; newline(); } // handle escape cases
<IN_STRING>\\t             { value += "\t"; }
<IN_STRING>\\\"             { value += "\""; }
<IN_STRING>\\\\            { value += "\\"; }
<IN_STRING>\\.            { error(linenum, colnum, "Illigal character escape."); }
<IN_STRING>[^\\\"]$ { BEGIN(0); error(linenum, colnum, "Line end encountered but string remains unterminated"); }
<IN_STRING>.              { value += yytext; }


"*/" { error(linenum, colnum, "Comment ended before it started"); }
"/*"          { BEGIN(IN_COMMENT); } //multiline comment start detection
<IN_COMMENT><<EOF>> { BEGIN(0); error(linenum, colnum, "Unclosed comment at the end of the file."); }
<IN_COMMENT>{
    "/*" { ++comment_depth; BEGIN(IN_COMMENT); } //handle nested comments, increase "nestiness" counter
    "*/" { if (comment_depth) --comment_depth; //handle nested comments, decrease "nestiness" counter
           else BEGIN(0); } //if it's not nested comment, get back to initial state
    \n   { newline(); } // explicit ignore case for newline because . does not account for that
    .    { } // ignore any other character in the comment
}

<<EOF>>			{	yyterminate(); }
.				{	error(linenum, colnum, string(YYText()) + " illegal token");}

%%

int string2int( string val )
{
	stringstream	ss(val);
	int				retval;

	ss >> retval;

	return retval;
}

void newline()
{
	linenum ++;
	colnum = 1;
	tokenCol = 1;
}

void error(int line, int col, string msg)
{
	errormsg.error(line, col, msg);
}
